package controllers;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import models.*;
import play.Logger;
import play.mvc.Controller;

public class Tweets extends Controller
{
	public static void index()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		List<Tweeter> followed = getFollowings(tweeter);
		List<Tweet> tweets = getTweets(tweeter);
		
		for (Tweeter tw : followed) 
		{
			tweets.addAll(getTweets(tw));
		}	
		
		render(tweeter, tweets);
	}
	
	static List<Tweeter> getFollowings(Tweeter tweeter)
	{	    		
		List<Following> allFollowings = Following.findAll();
		ArrayList<Tweeter> followed = new ArrayList<>();

		for (Following following : allFollowings)
			if (following.follower == tweeter)
			{
				followed.add(following.followee);
			}
		return followed;
	}

	static List<Tweet> getTweets(Tweeter tweeter)
	{
		List<Tweet> allTweets = Tweet.findAll();
		List<Tweet> tweets = new ArrayList<Tweet>();
		for (Tweet tweet : allTweets)
		{
			if (tweet.tweeter == tweeter) 
			{
				Logger.info("Tweet" + tweet);
				tweets.add(tweet); 
			}
		}
		return tweets;
	}

	public static void myTweets()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		List<Tweet> tweets = getTweets(tweeter);
		renderTemplate("Tweets/index.html", tweeter, tweets);
	}

	public static void tweetersTweets(String id)
	{
		Tweeter tweeter = Tweeter.findById(id);
		List<Tweet> tweets = getTweets(tweeter);
		renderTemplate("Tweets/index.html", tweeter, tweets);		
	}
	
	public static void tweet()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		Logger.info("Current tweeter " + tweeter.firstName);
		renderTemplate("Tweets/show.html");
	}

	public static void newTweet(String tweetText, String charCount)
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		Tweet tweet = new Tweet(tweeter, tweetText, charCount);
		tweet.tweeterTweetText = tweet.tweeter.firstName + " says " + tweet.tweetText;		
		Logger.info("Current tweeter " + tweeter.firstName);
		tweeter.tweets.add(tweet);
		tweet.tweeter = tweeter;
		Logger.info("tweet " + tweet);
		tweeter.save();
		tweet.save();
		index();
	}
}

