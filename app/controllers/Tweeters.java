package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import models.Following;
import models.Tweet;
import models.Tweeter;
import play.Logger;
import play.mvc.Controller;

public class Tweeters extends Controller
{
	public static void index()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		List<Following> followings = getFollowings(tweeter);
		List<Tweeter> tweeters = getTweeters(tweeter);

		render(tweeter, followings, tweeters);
	}

	static List<Following> getFollowings(Tweeter tweeter)
	{
		List<Following> allFollowings = Following.findAll();
		List<Following> followings = new ArrayList<>();

		for (Following following : allFollowings)
			if (following.follower == tweeter)
			{
				followings.add(following);
			}
		return followings;
	}

	static List<Tweeter> getTweeters(Tweeter tweeter)
	{
		List<Tweeter> followings = Tweets.getFollowings(tweeter);
		List<Tweeter> tweeters = Tweeter.findAll();
		tweeters.remove(tweeter);
		
		for (Tweeter twe : followings)
		{
			tweeters.remove(twe);
		}
		
		return tweeters;
	}


	public static void addFollow(String id)
	{
		Tweeter follower = Accounts.getCurrentTweeter();
		Tweeter followee = Tweeter.findById(id);
		follower.follow(followee);
		index();
	}

	public static void drop(String id)
	{
		Tweeter follower = Accounts.getCurrentTweeter();
		Tweeter followee = Tweeter.findById(id);
		follower.unfollow(followee);
		Logger.info("Dropping " + followee.email);
		index();
	}

	public static void edit()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		render(tweeter);
	}


	public static void changeDetails(String firstName, String lastName, String email, String password)
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();

		if (!firstName.isEmpty())
			tweeter.firstName = firstName;

		if (!lastName.isEmpty())
			tweeter.lastName = lastName;

		if (!email.isEmpty())
			tweeter.email = email;

		if (!password.isEmpty())
			tweeter.password = password;
		

		tweeter.save();
		index();
	}

}
