package models;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Tweet extends GenericModel
{
	@Id
	public String id;
	public String tweetText;
	public String charCount;
	public String tweeterTweetText;


	@ManyToOne
	public Tweeter tweeter;

	public Tweet(Tweeter tweeter, String tweetText, String charCount)
	{
		this.id       = UUID.randomUUID().toString();
		this.tweetText = tweetText;
		this.tweeter = tweeter;
		this.charCount = charCount;
	}

}