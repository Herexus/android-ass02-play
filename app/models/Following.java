package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Following extends GenericModel
{
	
	@Id
	public String id;
	
	@ManyToOne()
	public Tweeter follower;

	@ManyToOne()
	public Tweeter followee;
	
	public Following(Tweeter sourceTweeter, Tweeter targetTweeter)
	{
		this.id = UUID.randomUUID().toString();
		follower = sourceTweeter;
		followee = targetTweeter;
	}
}
