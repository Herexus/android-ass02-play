package models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.db.jpa.GenericModel;

@Entity
public class Tweeter extends GenericModel
{	
	@Id
	public String id;
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public boolean followed;

	@OneToMany(mappedBy="tweeter", cascade = CascadeType.ALL)
	public List<Tweet> tweets = new ArrayList<Tweet>();

	@OneToMany(mappedBy = "follower", cascade = CascadeType.ALL)
	public List<Following> followings = new ArrayList<Following>();

	public Tweeter ()
	{
		this.id = UUID.randomUUID().toString();
	}

	public static Tweeter findByEmail(String email)
	{
		return find("email", email).first();
	}

	public boolean checkPassword(String password)
	{
		return this.password.equals(password);
	}

	public String getFullName()
	{
		return firstName + " " + lastName;
	}

	private boolean followingsContains(Tweeter followed)
	{
		for (Following following : followings)
		{
			if (following.followee == followed)
			{
				return true;
			}
		}
		return false;
	}
	  
	public void follow(Tweeter followed)
	{
		if (followingsContains(followed))
		{
			this.followings = null;
		}
		else
		{
			Following following  = new Following(this, followed);
			followings.add(following);
			following.save();
			save();
		}
	}

	public void unfollow(Tweeter followed)
	{
		Following thisFollowing = null;

		for (Following f : followings)
		{
			if (f.followee == followed)
			{
				thisFollowing = f;
			}
		}
		followings.remove(thisFollowing);
		thisFollowing.delete();
		save();
	}
}

